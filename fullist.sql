-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2019 at 07:36 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `fullist`
--

CREATE TABLE `fullist` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(32) NOT NULL,
  `type` varchar(255) NOT NULL,
  `size` int(32) NOT NULL,
  `weight` int(11) NOT NULL,
  `height` int(32) NOT NULL,
  `width` int(32) NOT NULL,
  `length` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fullist`
--

INSERT INTO `fullist` (`id`, `sku`, `name`, `price`, `type`, `size`, `weight`, `height`, `width`, `length`) VALUES
(339, '333book', 'book333', 33, 'book', 0, 1, 0, 0, 0),
(340, '333book', 'book333', 33, 'book', 0, 1, 0, 0, 0),
(341, '334book', 'book334', 34, 'book', 0, 1, 0, 0, 0),
(342, '334book', 'book334', 34, 'book', 0, 1, 0, 0, 0),
(343, '444disc', 'disc44', 44, 'disc', 700, 0, 0, 0, 0),
(344, '444disc', 'disc44', 44, 'disc', 700, 0, 0, 0, 0),
(345, '445disc', 'disc445', 45, 'disc', 701, 0, 0, 0, 0),
(346, '445disc', 'disc445', 45, 'disc', 701, 0, 0, 0, 0),
(348, 'table001', 'table', 55, 'furniture', 0, 0, 22, 33, 44),
(349, 'table001', 'table', 55, 'furniture', 0, 0, 22, 33, 44),
(350, 'table001', 'table', 55, 'furniture', 0, 0, 22, 33, 44),
(351, 'table001', 'table', 55, 'furniture', 0, 0, 22, 33, 44);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fullist`
--
ALTER TABLE `fullist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fullist`
--
ALTER TABLE `fullist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=352;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
