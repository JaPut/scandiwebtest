<?php
class Product
{
    // database connection and table name
    private $conn;
    private $table_name = "fullist";
 
    // object properties
    public $id;
    public $sku;
    public $price;
    public $type;
    public $size;
    public $weight;
    public $height;
    public $width;
    public $length;
       
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // create product
    public function create()
    {      
        $query = "INSERT INTO " . $this->table_name . " SET
                sku=:sku, name=:name, price=:price, type=:type, size=:size,
                weight=:weight, height=:height, width=:width, length=:length";
         $stmt = $this->conn->prepare($query);
        // bind values 
        $stmt->bindParam(":sku", $_POST['sku']);
        $stmt->bindParam(":name", $_POST['name']);
        $stmt->bindParam(":price", $_POST['price']);
        $stmt->bindParam(":type", $_POST['type']);
        $stmt->bindParam(":size", $_POST['size']);
        $stmt->bindParam(":weight", $_POST['weight']);
        $stmt->bindParam(":height", $_POST['height']);
        $stmt->bindParam(":width", $_POST['width']);
        $stmt->bindParam(":length", $_POST['length']);
 
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    // select type disc product
    public function readDisc()
    {
        $query = "SELECT * FROM fullist WHERE type='disc' ORDER BY id DESC";
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        return $stmt;
    }

    // select type book product
    public function readBook()
    {
        $query = "SELECT * FROM fullist WHERE type='book' ORDER BY id DESC";
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        return $stmt;
    }

    // select type furniture product
    public function readFurniture()
    {
        $query = "SELECT * FROM fullist WHERE type='furniture' ORDER BY id DESC";
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
        return $stmt;
    }

    // delete selected product cards
    public function massDelete()
    {
        if(isset($_POST['mdel'])) {
        $checkbox = $_POST['check'];
        for($i=0;$i<count($checkbox);$i++) {
        $del_id = $checkbox[$i];
        $query = "DELETE FROM fullist WHERE id='".$del_id."'";
        $stmt4 = $this->conn->prepare( $query );
        $stmt4->execute();
        header("Location:index.php");
            }
        }
    }
}
?>
