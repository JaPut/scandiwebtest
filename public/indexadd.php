<?php
include_once '../config/database.php';
include_once '../classes/product.class.php';

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);
$product->create()
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link href="../styles/style.css" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="../script/main.js"></script>
    <title>Data add form</title>
</head>
<body>
     <!-- set page top part -->
    <form name="inputForm" action="" method="post"><br>
        <label class="alignleft">Product Add</label>
        <label class="alignright">
        <input type="submit" action=""class="styled" 
        id="click" name="button" value="Save"></label> 
        </label>
    <div class="clear"></div><br>
    <hr>
        <!-- preapare input fields -->
        <div class="tabalign">
            SKU: <input type="text" name="sku" required><br><br>
            Name: <input type="text" name="name" required><br><br>
            Price: <input type="text" name="price" required><br><br>
        </div><br><br><br><br><br><br><br><br>
        <!-- prepare special attribute fields -->
        <div class="swalign">
            Type Switcher <select name="type" id="selectone">
                <option value="0">Select product type</option>
                <option value="furniture">furniture</option>
                <option value="book">book</option>
                <option value="disc">dvd-disc</option>
            </select>
        </div><br>
        <div id="furniture" class='types'>
            <div class="atralign">
            <span>Height: </span> <input type="text" name="height"> cm<br><br>
            <span>Width: </span>  <input type="text" name="width"> cm<br><br>
            <span>Length: </span> <input type="text" name="length"> cm<br><br>
                <p>Dimensions in HxWxL format</p>
            </div>
        </div> 
        <div id="book" class='types'>
            <div class="atralign">
            <span>Weight: </span> <input type="text" 
               name="weight"> kg<br><br>
                <p>Please, provide total weight of book</p>
            </div>
        </div>
        <div id="disc" class='types'>
            <div class="atralign">
            <span>Size:</span> <input type="text" name="size"> MB<br><br>
                <p>Please, provide maximum size of disc</p>
            </div>
        </div>
    </div>
    </form>
</body>
</html>