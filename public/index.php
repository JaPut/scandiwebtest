<?php
include_once '../config/database.php';
include_once '../classes/product.class.php';
$database = new Database();
$db = $database->getConnection();
$product = new Product($db);

$stmt1 = $product->readDisc();
$num = $stmt1->rowCount();

$stmt2 = $product->readBook();
$num2 = $stmt2->rowCount();

$stmt3 = $product->readFurniture();
$num3 = $stmt3->rowCount();

$product->massDelete();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link href="../styles/style.css" rel="stylesheet"/>
    <title>Product show form</title>
</head>
<body>
    <!-- set page top part -->
    <form action="" method="post"><br>
        <label class="alignleft">Product List</label>
        <label class="alignright">Select All <input type="checkbox" 
        id="checkAl">
        <label class="showno">___ </label>
        <input class="styled" type="submit" name="mdel" 
        value="Delete All Selected">
        </label>
        <script src="../script/main.js"></script>
        <div class="clear";></div><br>
    <hr>
   <!-- create product cards -->
    <div class="container">
        <?php
            if($num > 0) {
            while ($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
        ?>
        <div class="col-md-3">
            <div class="productbox">
                    <div class="check">
                    <input type="checkbox" id="checkItem" name="check[]" 
                    value="<?php echo $row["id"]; ?>">
                    </div>
                <p class="pfonts">SKU: <?php echo $row["sku"]; ?></p>
                <p class="pfonts">Name: <?php echo $row["name"]; ?></p>
                <p class="pfonts">Price: <?php echo $row["price"]; ?> $</p>
                <p class="pfonts">Size: <?php echo $row["size"]; ?> MB</p>
            </div>
        <br>
        </div>
        <?php
		}
	    }
            if($num2 > 0) {
            while ($row = $stmt2->fetch(PDO::FETCH_ASSOC)) {
        ?>
        <div class="col-md-3">
            <div class="productbox">
                    <div class="check">
                    <input type="checkbox" id="checkItem" name="check[]" 
                    value="<?php echo $row["id"]; ?>">
                    </div>
                <p class="pfonts">SKU: <?php echo $row["sku"]; ?></p>
                <p class="pfonts">Name: <?php echo $row["name"]; ?></p>
                <p class="pfonts">Price: <?php echo $row["price"]; ?> $</p>
                <p class="pfonts">Weight: <?php echo $row["weight"]; ?> KG</p>
            </div>
        <br>
        </div>
        <?php
		}
		}
            if($num3 > 0) {
            while ($row = $stmt3->fetch(PDO::FETCH_ASSOC)) {
        ?>
        <div class="col-md-3">
            <div class="productbox">
                    <div class="check">
                    <input type="checkbox" id="checkItem" name="check[]" 
                    value="<?php echo $row["id"]; ?>">
                    </div>
                <p class="pfonts">SKU: <?php echo $row["sku"]; ?></p>
                <p class="pfonts">Name: <?php echo $row["name"]; ?></p>
                <p class="pfonts">Price: <?php echo $row["price"]; ?> $</p>
                <p class="pfonts">Dimensions: <?php echo $row["height"];
                 ?> x <?php echo $row["width"]; ?> x 
                 <?php echo $row["length"]; ?> cm</p>
            </div>
        <br>
		</div>
        <?php
		}
		}
        ?>
        <div style="clear:both"></div><br />
        <!-- avoid from empty checkbox post -->
	    <input hidden type="checkbox" name="check[]" checked>
    </form>
    </div>
</body>
</html>